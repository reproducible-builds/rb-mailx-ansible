#
# {{ ansible_managed }}
#

<Macro common-https-redirect $name>
    <VirtualHost *:80>
        ServerName $name
        ServerAdmin rb-core@reproducible-builds.org
        CustomLog ${APACHE_LOG_DIR}/$name/access.log combined
        ErrorLog ${APACHE_LOG_DIR}/$name/error.log
        Redirect permanent / https://$name/
    </VirtualHost>
</Macro>

<Macro common-https-conf $name>
    SSLEngine On
    SSLCertificateFile /var/lib/dehydrated/certs/$name/fullchain.pem
    SSLCertificateKeyFile /var/lib/dehydrated/certs/$name/privkey.pem

    Header always set Strict-Transport-Security "max-age=15768000"
</Macro>

<Macro CSP $img $font $style $script $media>
    Header always set Content-Security-Policy \
        "\
        default-src 'none'; \
        connect-src 'self'; \
        frame-src 'none'; \
        frame-ancestors 'none'; \
        img-src 'self' $img; \
        font-src 'self' $font; \
        style-src 'self' $style; \
        script-src 'self' $script; \
        media-src 'self' $media; \
        "
    Header always edit* Content-Security-Policy "\s+" " "
</Macro>

<Macro common-conf $name>
    ServerName $name
    ServerAdmin rb-core@reproducible-builds.org
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/$name/access.log combined
    ErrorLog ${APACHE_LOG_DIR}/$name/error.log

    # %{HOSTNAME} is set in envvars
    PassEnv HOSTNAME
    Header always set X-HostName "%{HOSTNAME}e"

    # forbid embedding in iframes
    Header always set X-Frame-Options DENY
    # enable browser-side XSS protection
    Header always set X-XSS-Protection "1; mode=block"
    # not load external CSSs and scripts unless the MIME type is correct
    Header always set X-Content-Type-Options "nosniff"
    # send full referrer on same origin, URL sans path on foreign origin
    Header always set Referrer-Policy "strict-origin-when-cross-origin"

    # Content-Secure-Policy
    Use CSP '' '' '' '' ''

    <Directory />
        Options FollowSymlinks
        AllowOverride None
        Require all denied
    </Directory>
</Macro>

<Macro simple-site $name>
    Use common-conf $name

    DocumentRoot /srv/$name/htdocs

    <Directory /srv/$name/htdocs/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        IndexOptions FancyIndexing
        Require all granted
    </Directory>
</Macro>
