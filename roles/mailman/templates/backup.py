#!/usr/bin/python3

#
# {{ ansible_managed }}
#

import os
import re
import sys
import datetime
import tempfile
import subprocess

from pathlib import Path

time_str = '%Y%m%dT%H%M%SZ'
reg = re.compile(r'^backup-rb-lists_(\d{8}T\d{6}Z)\.tar\.xz$')

now = datetime.datetime.now()
now_str = now.strftime(time_str)

backupdir = Path('/srv/mailman/backups/')
if not backupdir.exists():
    backupdir.mkdir()
if not backupdir.is_dir():
    print('backupdir exists but it is not a directory', file=sys.stderr)

bakfile = backupdir / 'backup-rb-lists_{}.tar.xz'.format(now_str)

with tempfile.NamedTemporaryFile(dir=backupdir, delete=False) as f:
    print('Creating the tarball…')
    try:
        subprocess.run(
            ('tar', 'cJf', f.name, '.'),
            cwd='/var/lib/mailman/',
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            check=True,
        )
    except subprocess.CalledProcessError as e:
        print('tar call failed ({}):'.format(e.returncode), file=sys.stderr)
        print(e.stdout.decode('utf-8'), file=sys.stderr)
        os.remove(f.name)
        sys.exit(1)
    try:
        print('Moving backup tarball in place…')
        os.rename(f.name, bakfile)
    except OSError as e:
        print('Failed to rename file in place', file=sys.stderr)
        os.remove(f.name)
        raise e

print('Backup completed!')
print()

# find old backup files
to_keep = []
to_remove = []
for f in backupdir.iterdir():
    if not f.is_file():
        continue
    r = re.match(reg, f.name)
    if not r:
        print(f'Not recognized: {f.name}')
        continue
    old_date = datetime.datetime.strptime(r.group(1), time_str)
    if now - old_date < datetime.timedelta(days=30):
        to_keep.append(f)
        continue
    if old_date.day == 1:
        to_keep.append(f)
        continue
    to_remove.append(f)
assert len(to_keep)+len(to_remove) == len(list(backupdir.iterdir()))
for f in to_remove:
    print(f'Removing {f}')
    f.unlink()


print("rsync to Mattia's place:")
try:
    p = subprocess.check_output(
        ('rsync', '-vrlptD', '--delete', f'{backupdir}{os.sep}',
         'zh2837@backuphost.mapreri.org:reproducible-builds/backup-rb-lists/'),
        stderr=subprocess.STDOUT,
    )
except subprocess.CalledProcessError as e:
    print('rsync to mattia failed:', file=sys.stderr)
    print(e.stdout.decode('utf-8'))
    sys.exit(1)
