#!/bin/bash
#
# {{ ansible_managed }}

set -e
set -u
set -o pipefail

OP="$1"
shift

_log () {
    echo " + (${OP}) $*"
}

deploy_cert () {
    _log "Reloading apache..."
    sudo apache2ctl graceful

    _log "Reloading postfix..."
    sudo service postfix reload
}


case "$OP" in
    deploy_challenge|clean_challenge)
        ;;
    deploy_cert)
        "$OP"
        ;;
    unchanged_cert|generate_csr|invalid_challenge|request_failure|startup_hook|exit_hook)
        ;;
    this_hookscript_is_broken__dehydrated_is_working_fine__please_ignore_unknown_hooks_in_your_script)
        ;;
    *)
        echo "WARN: unknown operation: $OP" >&2
        ;;
esac
